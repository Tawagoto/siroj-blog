// Modules //

const expressEdge = require('express-edge');
const express = require('express');
const edge = require('edge.js');
const app = new express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const storeArticle = require('./middleware/storeArticle')
const express_Session = require('express-session');
const connectMongo = require('connect-mongo');
const mongoStore = connectMongo(express_Session);
const connectFlash = require('connect-flash');
const env = require('dotenv').config();
console.log(process.env.DATABASE);

// Controllers //

const creerArticleController = require('./controllers/creerArticle');
const getArticleController = require('./controllers/getArticle');
const storeArticleController = require('./controllers/storeArticle');
const homePageController = require('./controllers/homePage');
const inscriptionController = require('./controllers/creerUtilisateur')
const storeUtilisateur = require('./controllers/storeUtilisateur');
const connexionController = require('./controllers/connexion');
const connexionUtilisateurController = require('./controllers/loginUtilisateur');
var upload_image = require("./public/js/image_upload.js");
const uploadImageController = require ('./controllers/uploadImage');

// Middle
const auth = require("./middleware/auth")
const redirectAuthOk = require('./middleware/redirectionAuthOk')

mongoose.connect(process.env.DATABASE, { useNewUrlParser: true })
    .then(() => 'Connecté à Mongo!')
    .catch(err => console.error('Erreur Mongo: ', err))
 //mongoose.set('debug', true);   

app.use(express.static('public'));
app.use(expressEdge);
app.use(fileUpload());
app.use(express_Session({
    secret: 'secret',
    store: new mongoStore({
        mongooseConnection: mongoose.connection
    })
}));

app.set('views', __dirname + '/views');
app.use('*', (req,res,next) =>{
    edge.global('auth', req.session.utilisateurId);
    next()
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(connectFlash());
app.use('/articles/store', storeArticle)
app.post("/utilisateurs/inscription", storeUtilisateur);
 
app.get("/", homePageController);
app.get("/article/:url", getArticleController);
app.get("/articles/nouveau", auth,creerArticleController);
app.post("/articles/store", storeArticleController);
app.post("/utilisateurs/connexion", redirectAuthOk,connexionUtilisateurController);
app.get("/auth/inscription",redirectAuthOk, inscriptionController);
app.get("/auth/connexion",redirectAuthOk,connexionController);
app.post("/image_upload", function (req, res) {
  upload_image(req, function(err, data) {
    console.log("passe dans la methode");
    if (err) {
      console.log("error dans la fonction");
      return res.status(404).end(JSON.stringify(err));
    }
    res.send(data);
  });
});


app.listen(process.env.PORT,() => {
    console.log(`"Le port d'écoute est le ${process.env.PORT}"`);
});