// Modèle Utilisateur

const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
 
const UtilisateurSchema = new mongoose.Schema({
    pseudo: {
        type: String,
        required: [true,'Merci de fournir un pseudonyme.'],
    },
    email: {
        type: String,
        required: [true,'Merci de fournir une adresse email valide.'],
        unique: true
    },
    password: {
        type: String,
        required: [true,'Merci de fournir un mot de passe.'],
    }
})
 
UtilisateurSchema.pre('save', function (next) {
    const utilisateur = this
 
    bcrypt.hash(utilisateur.password, 10, function (error, encrypted) {
        utilisateur.password = encrypted
        next()
    })
})
 
module.exports = mongoose.model('Utilisateur', UtilisateurSchema)