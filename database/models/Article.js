// Modèle Article

const mongoose = require('mongoose'); 
const ArticleSchema = new mongoose.Schema({
    titre: String,
    description: String,
    contenu: String,
    utilisateur: String,
    image: String,
    dateCreation: {
        type: Date,
        default: new Date()
    },
    url: String
});
 

const Article = mongoose.model('Article', ArticleSchema);
 
module.exports = Article;