const Utilisateur = require('../database/models/Utilisateur')
 
module.exports = (req, res, next) => {
    Utilisateur.findById(req.session.utilisateurId, (error, utilisateur) => {
        if (error || !utilisateur) {
            return res.redirect('/')
        }
 
        next()
    })
}