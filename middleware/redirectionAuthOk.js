const Utilisateur = require('../database/models/Utilisateur');
module.exports = (req,res,next) =>{
   if(req.session.utilisateurId){
       return res.redirect('/')
   } 
   next()
}