module.exports = (req, res, next) => {
    if (!req.files || !req.body.utilisateur || !req.body.titre || !req.body.description || !req.body.contenu) {
        return res.redirect('/articles/nouveau')
    }
    req.body.url = req.body.titre.replace(/ /g, '-');
    next()
}