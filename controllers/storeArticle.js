const path = require('path');
const Article = require('../database/models/Article');

module.exports = (req,res) => {
    const { image } = req.files;
    image.mv(path.resolve(__dirname,'..','public/img', image.name), (error) =>{
        Article.create({
            ...req.body,
            image: `/img/${image.name}`
        }, (error, post) => {
            // console.log(req.body);
            res.redirect('/');
        });
    }); 
};
