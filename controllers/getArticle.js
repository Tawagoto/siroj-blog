const Article = require('../database/models/Article');
const readingTime = require('reading-time');
module.exports = async (req, res) => {
    const article = await Article.findOne({ url : req.params.url });
    const readTime = readingTime(article.contenu)
    console.log(readTime)
    res.render("article", {
        article,
        readTime
    });
} 
