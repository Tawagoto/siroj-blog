const Utilisateur = require('../database/models/Utilisateur')
 
module.exports = (req, res) => {
    Utilisateur.create(req.body, (error, utilisateur) => {
        if (error) {
            const registrationErrors = Object.keys(error.errors).map(key => error.errors[key].message);
            req.flash('registrationErrors', registrationErrors);
            return res.redirect('/auth/inscription')
        }
        res.redirect('/')
    })
}