
module.exports = (req, res) => {

    if(req.session.utilisateurId){
        return res.render("creer");
    } else return res.render("connexion");
    
};