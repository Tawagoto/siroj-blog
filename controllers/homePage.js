const Article = require('../database/models/Article');
const moment = require('moment');
moment.locale('fr');

module.exports = async (req, res) => {
    const articles = await Article.find({});
    res.render("index", {
        articles,
        moment
    });
}