const bcrypt = require('bcrypt')
const Utilisateur = require('../database/models/Utilisateur')
 
module.exports = (req, res) => {
    const {
        email,
        password
    } = req.body;
    // try to find the user
    Utilisateur.findOne({
        email
    }, (error, utilisateur) => {
        if (utilisateur) {
            // compare passwords.
            bcrypt.compare(password, utilisateur.password, (error, same) => {
                if (same) {
                    req.session.utilisateurId = utilisateur._id;
                    res.redirect('/')
                } else {
                    res.redirect('/auth/connexion')
                }
            })
        } else {
            return res.redirect('/auth/connexion')
        }
    })
}